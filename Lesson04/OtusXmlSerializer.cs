﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Lesson04
{
    class OtusXmlSerializer<T> : ISerializer<T>
    {
        private readonly XmlWriterSettings xmlWriterSettings;
        private IExtendedXmlSerializer serializer;

        public OtusXmlSerializer()
        {
            xmlWriterSettings = new XmlWriterSettings { Indent = true };
            serializer = new ConfigurationContainer()
              .UseAutoFormatting()
              .UseOptimizedNamespaces()
              .EnableImplicitTyping(typeof(T))
              .Create();
        }
        public T Deserialize<T>(string data)
        {
            return serializer.Deserialize<T>(data);
        }

        public string Serialize<T>(T item)
        {
            return serializer.Serialize(xmlWriterSettings, item);
        }
    }
}
