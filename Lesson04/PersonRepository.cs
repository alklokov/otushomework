﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson04
{
    class PersonRepository : IRepository<Person>
    {
        private List<Person> staff = new List<Person> {new Person("Петров", "Петр", 25),
                                                       new Person("Иванов", "Петр", 35),
                                                       new Person("Иванов", "Иван", 45)};

        public void Add(Person item)
        {
            staff.Add(item);
        }

        public IEnumerable<Person> GetAll()
        {
            foreach (var item in staff)
                yield return item;
        }

        public Person GetOne(Func<Person, bool> predicate)
        {
            foreach (var item in staff)
                if (predicate(item))
                    return item;
            return null;
        }
    }
}
