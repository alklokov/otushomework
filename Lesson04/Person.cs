﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson04
{
    class Person
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public int Age { get; set; }

        public Person()
        {}
        public Person(string lastName, string firstName, int age)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
        }
    }
}
