﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lesson04
{
    class PersonSorter : ISorter<Person>
    {
        public IEnumerable<Person> Sort(IEnumerable<Person> notSortedItems)
        {
            return notSortedItems.OrderBy(p => p.LastName + p.FirstName);
        }
    }
}
