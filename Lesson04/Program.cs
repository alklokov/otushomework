﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Lesson04
{
    class Program
    {

        //static Person[] testStaff = {new Person("Петров", "Петр", 25),
        //                             new Person("Иванов", "Петр", 35),
        //                             new Person("Иванов", "Иван", 45)};

        static void Main(string[] args)
        {
            var sorter = new PersonSorter();
            var rep = new PersonRepository();
            
            Console.WriteLine("*** XmlSerializer ***\n");
            TestSerializeDeserialize(rep.GetAll().ToArray(), new OtusXmlSerializer<Person>(), sorter);

            Console.WriteLine("\n\n*** JsonSerializer ***\n");
            TestSerializeDeserialize(rep.GetAll().ToArray(), new OtusJsonSerializer<Person>(), sorter);

            Console.ReadLine();
        }

        static void TestSerializeDeserialize(Person[] staff, ISerializer<Person> serializer, ISorter<Person> sorter)
        {
            string data = serializer.Serialize(staff);
            Console.WriteLine("Source data:\n" + data);
            var arr1 = serializer.Deserialize<Person[]>(data);
            var arr2 = sorter.Sort(arr1).ToArray();
            data = serializer.Serialize(arr2);
            Console.WriteLine("\nSorted data:\n" + data);
        }

    }
}
