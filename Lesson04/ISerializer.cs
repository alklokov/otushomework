﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson04
{
    interface ISerializer<T>
    {
        string Serialize<T>(T item);
        T Deserialize<T>(string data);
    }
}
