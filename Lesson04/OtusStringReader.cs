﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Lesson04
{
    class OtusStringReader<T> : IEnumerable<T>, IDisposable
    {
        string data;
        ISerializer<T> serializer;

        public OtusStringReader(string data, ISerializer<T> serializer)
        {
            this.data = data;
            this.serializer = serializer;
        }

        public IEnumerator<T> GetEnumerator()
        {
            var arr = serializer.Deserialize<T[]>(data);
            foreach(var item in arr)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Dispose()
        {
            data = null;
        }
    }
}
