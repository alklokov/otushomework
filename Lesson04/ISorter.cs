﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson04
{
    interface ISorter<T>
    {
        IEnumerable<T> Sort(IEnumerable<T> notSortedItems);
    }
}
