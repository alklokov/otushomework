﻿using FacadePattern;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace FacadeTest
{
    public partial class FacadeTest : Form
    {
        public FacadeTest()
        {
            InitializeComponent();
        }

        private void btFillImage_Click(object sender, EventArgs e)
        {
            //Запрашиваем у пользователя путь к папке с фото
            openImageDialog.InitialDirectory = GetImageDir();
            if (openImageDialog.ShowDialog() != DialogResult.OK) return;

            string dstFile = "temp.jpg";

            //Обращение к фасадному классу
            var maker = new MotivatorMaker() { Height = pictureMain.Height, Width = pictureMain.Width };
            if (maker.MakeMotivator(openImageDialog.FileName, txtPhrase.Text, dstFile))
                ShowImage(dstFile);
            else
                MessageBox.Show(maker.ErrorDesctiption, "Ошибка!");
            return;
        }

        private string GetImageDir()
        {
            var imgPath = Environment.CurrentDirectory;
            imgPath = Directory.GetParent(imgPath).ToString();
            imgPath = Directory.GetParent(imgPath).ToString();
            imgPath = Directory.GetParent(imgPath).ToString();
            return Path.Combine(imgPath, "Images");
        }

        private void ShowImage(string imageFile)
        {
            try
            {
                var img = Image.FromFile(imageFile);
                pictureMain.Image = new Bitmap(img);
                img.Dispose();  //Освобождаем файл для последующих сохранений
            }
            catch (Exception)
            {
            }
        }
    }
}
