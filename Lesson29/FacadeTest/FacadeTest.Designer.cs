﻿namespace FacadeTest
{
    partial class FacadeTest
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureMain = new System.Windows.Forms.PictureBox();
            this.btFillImage = new System.Windows.Forms.Button();
            this.openImageDialog = new System.Windows.Forms.OpenFileDialog();
            this.txtPhrase = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureMain)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureMain
            // 
            this.pictureMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureMain.Location = new System.Drawing.Point(14, 14);
            this.pictureMain.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureMain.Name = "pictureMain";
            this.pictureMain.Size = new System.Drawing.Size(519, 505);
            this.pictureMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureMain.TabIndex = 11;
            this.pictureMain.TabStop = false;
            // 
            // btFillImage
            // 
            this.btFillImage.Location = new System.Drawing.Point(611, 62);
            this.btFillImage.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btFillImage.Name = "btFillImage";
            this.btFillImage.Size = new System.Drawing.Size(88, 27);
            this.btFillImage.TabIndex = 12;
            this.btFillImage.Text = "Test";
            this.btFillImage.UseVisualStyleBackColor = true;
            this.btFillImage.Click += new System.EventHandler(this.btFillImage_Click);
            // 
            // openImageDialog
            // 
            this.openImageDialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF";
            // 
            // txtPhrase
            // 
            this.txtPhrase.Location = new System.Drawing.Point(550, 16);
            this.txtPhrase.Name = "txtPhrase";
            this.txtPhrase.Size = new System.Drawing.Size(214, 23);
            this.txtPhrase.TabIndex = 13;
            this.txtPhrase.Text = "Красота спасет мир!";
            // 
            // FacadeTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 531);
            this.Controls.Add(this.txtPhrase);
            this.Controls.Add(this.btFillImage);
            this.Controls.Add(this.pictureMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FacadeTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Facade Test";
            ((System.ComponentModel.ISupportInitialize)(this.pictureMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureMain;
        private System.Windows.Forms.Button btFillImage;
        private System.Windows.Forms.OpenFileDialog openImageDialog;
        private System.Windows.Forms.TextBox txtPhrase;
    }
}

