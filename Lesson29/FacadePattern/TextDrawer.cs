﻿using System.Drawing;

namespace FacadePattern
{
    /// <summary>
    /// Класс, реализующий функцию отрисовки текста
    /// </summary>
    public class TextDrawer : IDrawer
    {
        /// <summary>
        /// Цвет текста
        /// </summary>
        public Brush TextColor { get; set; } = new SolidBrush(Color.Black);
        /// <summary>
        /// Шрифт
        /// </summary>
        public Font TextFont { get; set; } = new Font("Verdana", 20, FontStyle.Bold | FontStyle.Italic);
        /// <summary>
        /// Выравнивание текста
        /// </summary>
        public StringFormat TextAlignment { get; set; } = new StringFormat
        {
            Alignment = StringAlignment.Center,
            LineAlignment = StringAlignment.Center
        };

        private readonly Graphics graphics;
        private readonly string text;

        public TextDrawer(Graphics graphics, string text)
        {
            this.graphics = graphics;
            this.text = text;
        }

        public void Draw(int x, int y, int width, int height)
        {
            var textRect = new RectangleF(x, y, width, height);
            graphics.DrawString(text, TextFont, TextColor, textRect, TextAlignment);
        }



    }
}
