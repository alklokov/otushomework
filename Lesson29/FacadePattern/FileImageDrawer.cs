﻿using System;
using System.Drawing;

namespace FacadePattern
{
    /// <summary>
    /// Класс, реализующий функцию отрисовки картинки из файла
    /// </summary>
    public class FileImageDrawer : IDrawer
    {
        private readonly Graphics graphics;
        private readonly string imageFile;

        public FileImageDrawer(Graphics graphics, string imageFile)
        {
            this.graphics = graphics;
            this.imageFile = imageFile;
        }

        /// <summary>
        /// Рисуем изображение из файла в указанных координатах с сохранением соотношения сторон
        /// </summary>
        public void Draw(int x, int y, int width, int height)
        {
            var pic = new Bitmap(imageFile);
            var ratio = Math.Min(((double)width) / pic.Width, ((double)height) / pic.Height);
            int dx = (width - (int)(ratio * pic.Width)) / 2;
            int dy = (height - (int)(ratio * pic.Height)) / 2;
            var picRect = new RectangleF(x + dx, y + dy, width - 2 * dx, height - 2 * dy);
            graphics.DrawImage(pic, picRect);
        }
    }
}
