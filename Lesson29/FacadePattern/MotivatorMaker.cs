﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace FacadePattern
{
    /// <summary>
    /// Фасадный класс для создания картинки-мотиватора на основе картинки и текста
    /// </summary>
    public class MotivatorMaker
    {
        /// <summary>
        /// Ширина итоговой картинки
        /// </summary>
        public int Width { get; set; } = 500;
        /// <summary>
        /// Высота итоговой картинки
        /// </summary>
        public int Height { get; set; } = 500;
        /// <summary>
        /// Отступ по краям картинки
        /// </summary>
        public int Padding { get; set; } = 20;
        /// <summary>
        /// Цвет фона
        /// </summary>
        public Color BackgroungColor { get; set; } = Color.Beige;
        /// <summary>
        /// Высота области текста
        /// </summary>
        public int TextHeight { get; set; } = 50;
        /// <summary>
        /// Цвет текста
        /// </summary>
        public Color TextColor { get; set; } = Color.Black;
        public string ErrorDesctiption { get; private set; }

        public bool MakeMotivator(string pictureFile, string text, string resultFile)
        {
            try
            {
                //Создаем поле указанного размера и заливаем его указанным цветом
                var image = new Bitmap(Width, Height);
                var graph = Graphics.FromImage(image);
                graph.Clear(BackgroungColor);

                //Добавляем на рисунок текст
                IDrawer textDrawer = new TextDrawer(graph, text);
                textDrawer.Draw(0, Height - TextHeight, Width, TextHeight);

                //Добавляем на рисунок картинку
                IDrawer imageDrawer = new FileImageDrawer(graph, pictureFile);
                imageDrawer.Draw(Padding, Padding, Width - 2 * Padding, Height - TextHeight - 2 * Padding);

                //Сохраняем в файл
                image.Save(resultFile, ImageFormat.Jpeg);
                return true;
            }
            catch (Exception ex)
            {
                ErrorDesctiption = ex.Message;
                return false;
            }
        }
    }
}
