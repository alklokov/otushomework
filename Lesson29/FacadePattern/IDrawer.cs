﻿namespace FacadePattern
{
    /// <summary>
    /// Отрисовка чего-либо в прямоугольнике с указанными координатами
    /// </summary>
    public interface IDrawer
    {
        void Draw(int x, int y, int width, int height);
    }
}