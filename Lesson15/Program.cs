﻿using System;

namespace Lesson15
{
    class Program
    {
        static void Main(string[] args)
        {
            TestGetMax();
            TestFileSearcher();
        }

        static void TestGetMax()
        {
            Person[] persons = { 
                new Person("Alex", 75.5f, 175f), 
                new Person("Petr", 87f, 180f), 
                new Person("Vasiliy", 70f, 185f) 
            };
            Console.WriteLine("The heaviest person:\t" + persons.GetMax(p => p.Weight).ToString());
            Console.WriteLine("The tallest person:\t" + persons.GetMax(p => p.Height).ToString());
            Console.WriteLine();
        }

        static void TestFileSearcher()
        {
            FileSearcher searcher = new FileSearcher();
            searcher.SearchFilesInFolder(Environment.CurrentDirectory);
            Console.WriteLine();
            searcher.SearchFilesInFolder("SomeWrongFolder");
            Console.WriteLine();
        }
    }


}
