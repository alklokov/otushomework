﻿using System;
using System.IO;

namespace Lesson15
{
    public class FileFinder
    {
        public event EventHandler<FileArgs> FileFound;
        public event EventHandler<FileFinderError> Error;

        public void FindAllFilesInFolder(string folder)
        {
            try
            {
                foreach(string file in Directory.EnumerateFiles(folder))
                {
                    if (FileFound == null)
                        return; //Или нет подписчиков, или отписались в процессе, тогда завершаем перебор
                    FileFound(this, new FileArgs(file));
                }
                //Пустая строка - признак завершения поиска
                FileFound(this, new FileArgs(""));
            }
            catch (Exception ex)
            {
                Error?.Invoke(this, new FileFinderError(ex.Message));
            }
        }
    }
}
