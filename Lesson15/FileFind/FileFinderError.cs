﻿using System;

namespace Lesson15
{
    public class FileFinderError : EventArgs
    {
        public string Error { get; private set; }
        public FileFinderError(string error)
        {
            Error = error;
        }
    }
}