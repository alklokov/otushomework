﻿using System;

namespace Lesson15
{
    public class FileSearcher
    {
        private FileFinder fileFinder;
        public FileSearcher()
        {
            fileFinder = new FileFinder();
            fileFinder.Error += OnFileFinderError;
        }

        public void SearchFilesInFolder(string folder)
        {
            fileFinder.FileFound += OnFileFound;
            Console.WriteLine($"Start searching files in folder: \"{folder}\"");
            fileFinder.FindAllFilesInFolder(folder);
        }

        private void OnFileFound(object sender, FileArgs e)
        {
            if (e.FileName == "") FinishSearch();
            Console.WriteLine($"Found file: {e.FileName}.\tPress RightArrow to proceed");
            if (Console.ReadKey(true).Key != ConsoleKey.RightArrow)
                FinishSearch();
        }

        private void OnFileFinderError(object sender, FileFinderError e)
        {
            fileFinder.FileFound -= OnFileFound;
            Console.WriteLine("FindFiles error: " + e.Error);
        }

        private void FinishSearch()
        {
            fileFinder.FileFound -= OnFileFound;
            Console.WriteLine("Search is finished");
        }
    }
}
