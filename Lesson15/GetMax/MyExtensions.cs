﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lesson15
{
    static class MyExtensions
    {
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class
        {
            return e.OrderByDescending(x => getParametr(x)).FirstOrDefault(); 
        }
    }
}
