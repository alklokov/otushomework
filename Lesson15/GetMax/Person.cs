﻿namespace Lesson15
{
    class Person
    {
        public string Name { get; private set; }
        public float Weight { get; private set; }
        public float Height { get; private set; }
        public Person(string name, float weight, float height)
        {
            Name = name;
            Weight = weight;
            Height = height;
        }

        public override string ToString()
        {
            return $"Name: {Name}, Weight: {Weight}, Height: {Height}";
        }
    }
}
