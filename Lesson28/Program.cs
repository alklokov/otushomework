﻿using System;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            var test = new PrototypeTest();
            test.RunTest();

            Console.ReadLine();
        }
    }
}
