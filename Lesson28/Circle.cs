﻿namespace Prototype
{
    /// <summary>
    /// Окружность - производный класс от фигур.
    /// </summary>
    class Circle : Shape
    {
        protected int radius;

        public Circle(Point center, int radius) : base(center)
            => this.radius = radius;

        //Конструктор, принимающий сам объект, для удобства клонирования
        private Circle(Circle circle) : this(circle.center, circle.radius) { }

        /// <summary>
        /// Увеличить радиус окружности
        /// </summary>
        public void Enlarge(int delta)
            => radius += delta;

        public override Shape MyClone()
            => new Circle(this);

        public override object Clone()
            => MyClone();

        public override string ToString()
            => $"Circle with center {center} and radius {radius}";

    }
}
