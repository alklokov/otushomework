﻿using System;
using System.Collections.Generic;

namespace Prototype
{
    class PrototypeTest
    {
        private List<Shape> shapes;
        private List<Shape> myClones;
        private List<Shape> clones;

        public PrototypeTest()
            => InitShapes();

        /// <summary>
        /// Тестируем процедуру клонирования
        /// </summary>
        public void RunTest()
        {
            InitShapes();       //Создаем начальный список фигур
            MyCloneShapes();    //Клонируем список фигур с помощью IMyCloneable
            CloneShapes();      //Клонируем список фигур с помощью ICloneable
            PrintAllShapes();

            ChangeClones();     
            Console.WriteLine("\nВносим изменения в клонированные фигуры\n");
            PrintAllShapes();
        }


        //Создаем начальный список фигур
        private void InitShapes()
        {
            shapes = new List<Shape>();
            var center = new Point(1, 1);
            shapes.Add(new Shape(center));
            shapes.Add(new Circle(center, 10));
            shapes.Add(new ColorCircle(center, 10, "green"));
        }

        //Клонируем список фигур с помощью IMyCloneable
        private void MyCloneShapes()
        {
            myClones = new List<Shape>();
            foreach (var shape in shapes)
                myClones.Add(shape.MyClone());
        }

        //Клонируем список фигур с помощью ICloneable
        private void CloneShapes()
        {
            clones = new List<Shape>();
            foreach (var shape in shapes)
                clones.Add((Shape)shape.Clone());
        }

        //Изменяем клонированные фигуры
        private void ChangeClones()
        {
            ((Circle)myClones[1]).Enlarge(5);
            ((Circle)myClones[2]).Enlarge(5);
            ((ColorCircle)myClones[2]).SetColor("blue");

            ((Circle)clones[1]).Enlarge(10);
            ((Circle)clones[2]).Enlarge(10);
            ((ColorCircle)clones[2]).SetColor("red");
        }

        private void PrintAllShapes()
        {
            Console.WriteLine("Исходные фигуры:");
            PrintShapes(shapes);
            Console.WriteLine("\nФигуры, склонированные с помощью IMyCloneable:");
            PrintShapes(myClones);
            Console.WriteLine("\nФигуры, склонированные с помощью ICloneable:");
            PrintShapes(clones);
        }

        private void PrintShapes(List<Shape> shapes)
        {
            foreach (var shape in shapes) 
                Console.WriteLine(shape);
        }
    }
}
