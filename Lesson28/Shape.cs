﻿using System;

namespace Prototype
{
    /// <summary>
    /// Базовый класс для фигур. Имеет координату центра
    /// </summary>
    class Shape : IMyCloneable<Shape>, ICloneable
    {
        protected Point center; 

        public Shape(Point center)
            => this.center = center;

        //Конструктор, принимающий сам объект, для удобства клонирования
        private Shape(Shape shape) : this(shape.center) {}

        public virtual Shape MyClone()
            => new Shape(this);

        public virtual object Clone()
            => MyClone();

        public override string ToString()
            => $"Shape with center {center}";
    }
}
