﻿namespace Prototype
{
    /// <summary>
    /// Точка с координатами X,Y
    /// </summary>
    class Point
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
            => $"(X:{X}, Y:{Y})";
    }
}
