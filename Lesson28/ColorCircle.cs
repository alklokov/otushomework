﻿namespace Prototype
{
    /// <summary>
    /// Цветная окружность - производный класс от окружности
    /// </summary>
    class ColorCircle : Circle
    {
        protected string color;

        public ColorCircle(Point center, int radius, string color) : base(center, radius)
            => this.color = color;

        //Конструктор, принимающий сам объект, для удобства клонирования
        private ColorCircle(ColorCircle colorCircle) : this(colorCircle.center, colorCircle.radius, colorCircle.color) { }

        /// <summary>
        /// Изменить цвет фигуры
        /// </summary>
        /// <param name="color"></param>
        public void SetColor(string color)
            => this.color = color;

        public override Shape MyClone()
            => new ColorCircle(this);

        public override object Clone()
            => MyClone();

        public override string ToString()
            => $"ColorCircle with center {center}, radius {radius} and color {color}";
    }
}
