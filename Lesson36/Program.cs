﻿namespace Solid
{
    class Program
    {
        static void Main(string[] args)
        {
            var dataProcessor = new DataProcessor(
                new ConsoleNumberInputInteractive(),
                new ConsoleNumberOutputInteractive()
            );

            dataProcessor.DataAction = n => n * n;
            dataProcessor.Run();

            dataProcessor.DataAction = n => n * 2;
            dataProcessor.Run();
        }
    }
}
