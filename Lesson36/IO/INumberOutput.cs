﻿using System.Collections.Generic;

namespace Solid
{
    public interface INumberOutput
    {
        void WriteData(List<int> numList);
    }
}
