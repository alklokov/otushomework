﻿using System;
using System.Collections.Generic;

namespace Solid
{
    public class ConsoleNumberOutputInteractive : ConsoleNumberOutput
    {
        public override void WriteData(List<int> dataList)
        {
            Console.WriteLine("Обработанные данные:");
            base.WriteData(dataList);
        }
    }
}
