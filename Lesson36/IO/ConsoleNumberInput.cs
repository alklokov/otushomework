﻿using System;
using System.Collections.Generic;

namespace Solid
{
    public class ConsoleNumberInput : INumberInput
    {
        public virtual List<int> GetData()
        {
            var dataList = new List<int>();
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out int num))
                    dataList.Add(num);
                else
                    break;
            }
            return dataList;
        }
    }
}
