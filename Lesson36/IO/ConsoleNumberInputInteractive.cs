﻿using System;
using System.Collections.Generic;

namespace Solid
{
    public class ConsoleNumberInputInteractive : ConsoleNumberInput
    {
        public override List<int> GetData()
        {
            Console.WriteLine("Введите через Enter несколько целых чисел или пустую строку для выхода:");
            return base.GetData();
        }
    }
}
