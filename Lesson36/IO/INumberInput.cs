﻿using System.Collections.Generic;

namespace Solid
{
    public interface INumberInput
    {
        List<int> GetData();
    }
}
