﻿using System;
using System.Collections.Generic;

namespace Solid
{
    public class ConsoleNumberOutput : INumberOutput
    {
        public virtual void WriteData(List<int> dataList)
        {
            foreach (var n in dataList)
                Console.Write($"{n} ");
            Console.WriteLine();
        }
    }
}
