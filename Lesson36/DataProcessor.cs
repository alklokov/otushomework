﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Solid
{
    public class DataProcessor
    {
        private INumberInput input;
        private INumberOutput output;
        private List<int> data;

        public Func<int, int> DataAction { get; set; } = (n => n);

        public DataProcessor(INumberInput input, INumberOutput output)
        {
            this.input = input;
            this.output = output;
        }

        public void Run()
        {
            data = input.GetData();
            data = data.Select(DataAction).ToList();
            output.WriteData(data);
        }


    }
}
