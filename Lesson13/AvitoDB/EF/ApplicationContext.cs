﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvitoDB
{
    class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Announcement> Announcements { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<City> Cities { get; set; }

        public ApplicationContext(DbContextOptions options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=AvitoDb;Username=postgres;Password=postgres;Pooling=true;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(ConfigureUser);
            modelBuilder.Entity<Review>(ConfigureReview);
            modelBuilder.Entity<Announcement>(ConfigureAnnouncement);
        }


        //Конфигурация сущности User
        private void ConfigureUser(EntityTypeBuilder<User> builder)
        {
            builder.HasAlternateKey(u => u.Email);
            builder.HasOne(u => u.City).WithMany(c => c.Users).OnDelete(DeleteBehavior.SetNull);
        }

        //Конфигурация сущности Review
        private void ConfigureReview(EntityTypeBuilder<Review> builder)
        {
            builder.HasOne(r => r.Author).WithMany(u => u.AuthorOfReviews).OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(r => r.Seller).WithMany(u => u.ReviewsAboutUser).OnDelete(DeleteBehavior.Cascade);
        }

        //Конфигурация сущности Announcement
        private void ConfigureAnnouncement(EntityTypeBuilder<Announcement> builder)
        {
            builder.Property(a => a.Actual).HasDefaultValue(true);
            builder.Property(a => a.PublicationDate).HasDefaultValueSql("CURRENT_TIMESTAMP");
        }
    }
}
