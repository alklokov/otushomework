﻿using System;
using System.Collections.Generic;
using Npgsql;

namespace AvitoDB
{
    class RepositoryADO : IRepository
    {
        private readonly NpgsqlConnection connection;
        private bool disposed = false;

        public RepositoryADO(string connectionString)
        {
            connection = new NpgsqlConnection(connectionString);
        }

        public string ErrorDescription { get; private set; }
        public string Title { get => "ADO NET"; }

        public bool AddAnnouncement(Announcement ann)
        {
            bool res = false;
            try
            {
                connection.Open();
                var cmd = new NpgsqlCommand(
                    "INSERT INTO \"Announcements\" (\"Title\", \"Description\", \"Price\", \"UserId\") " +
                    "VALUES(@title, @descr, @price, @userid) returning \"Id\";", connection);
                cmd.Parameters.AddWithValue("@title", ann.Title);
                cmd.Parameters.AddWithValue("@descr", ann.Description);
                cmd.Parameters.AddWithValue("@price", ann.Price);
                cmd.Parameters.AddWithValue("@userid", ann.User.Id);
                ann.Id = (int)cmd.ExecuteScalar();

                //Добавляем категории товара
                if (ann.Categories != null)
                    foreach (var cat in ann.Categories)
                        AddAnnouncementCategory(ann.Id, cat.Id);
                res = true;
            }
            catch (Exception ex)
            {
                ErrorDescription = ex.Message;
            }
            finally
            {
                connection.Close();
            }
            return res;
        }

        public bool DeleteAnnouncement(Announcement ann)
        {
            bool res = false;
            try
            {
                connection.Open();
                var cmd = new NpgsqlCommand("DELETE FROM \"Announcements\" AS a WHERE a.\"Id\" = @id;", connection);
                cmd.Parameters.AddWithValue("@id", ann.Id);
                cmd.ExecuteNonQuery();
                res = true;
            }
            catch (Exception ex)
            {
                ErrorDescription = ex.Message;
            }
            finally
            {
                connection.Close();
            }
            return res;
        }

        public IEnumerable<Announcement> GetAllUserActualAnnouncemets(int userId)
        {
            var anns = new List<Announcement>();
            try
            {
                connection.Open();
                var cmd = new NpgsqlCommand("SELECT * FROM \"Announcements\" AS a WHERE a.\"Actual\" AND a.\"UserId\" = @uid;", connection);
                cmd.Parameters.AddWithValue("@uid", userId);
                using var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    anns.Add(new Announcement
                    {
                        Id = (int)reader["Id"],
                        Title = reader["Title"]?.ToString(),
                        Description = reader["Description"]?.ToString(),
                        Price = (decimal)reader["Price"],
                        PublicationDate = (DateTime)reader["PublicationDate"],
                        Categories = GetAllCategoriesByAnnouncemetId((int)reader["Id"])
                    });
                }
            }
            catch (Exception ex)
            {
                ErrorDescription = ex.Message;
                anns = null;
            }
            finally
            {
                connection.Close();
            }
            return anns;
        }

        public IEnumerable<User> GetAllUsers()
        {
            var users = new List<User>();
            try
            {
                connection.Open();
                var cmd = new NpgsqlCommand(
                    "SELECT u.*, c.\"CityName\" FROM \"Users\" AS u LEFT JOIN \"Cities\" AS c ON u.\"CityId\"=c.\"Id\";", 
                    connection);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        users.Add(new User
                        {
                            Name = reader["Name"]?.ToString(),
                            Email = reader["Email"]?.ToString(),
                            Phone = reader["Phone"]?.ToString(),
                            City = reader["CityName"] == null ? null : new City { CityName = reader["CityName"].ToString() }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorDescription = ex.Message;
                users = null;
            }
            finally
            {
                connection.Close();
            }
            return users;
        }

        public Category GetCategoryByTitle(string title)
        {
            Category cat = null;
            try
            {
                connection.Open();
                var cmd = new NpgsqlCommand(
                    "SELECT * FROM \"Categories\" AS c WHERE c.\"Title\" = @title;",
                    connection);
                cmd.Parameters.AddWithValue("@title", title);
                using var reader = cmd.ExecuteReader();
                reader.Read();
                cat = new Category
                {
                    Id = (int)reader["Id"],
                    Title = reader["Title"]?.ToString()
                };
            }
            catch (Exception ex)
            {
                ErrorDescription = ex.Message;
            }
            finally
            {
                connection.Close();
            }
            return cat;
        }

        public User GetUserByLoginPwd(string login, string password)
        {
            User user = null;
            try
            {
                connection.Open();
                var cmd = new NpgsqlCommand(
                    "SELECT u.*, c.\"CityName\" FROM \"Users\" AS u LEFT JOIN \"Cities\" AS c ON u.\"CityId\"=c.\"Id\"" +
                    "WHERE u.\"Email\" = @email AND \"Password\" = @pwd;",
                    connection);
                cmd.Parameters.AddWithValue("@email", login);
                cmd.Parameters.AddWithValue("@pwd", password);
                using (var reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    user = new User
                    {
                        Id = (int)reader["Id"],
                        Name = reader["Name"]?.ToString(),
                        Email = reader["Email"]?.ToString(),
                        Phone = reader["Name"]?.ToString(),
                        City = reader["CityName"] == null ? null : new City { CityName = reader["CityName"].ToString() }
                    };
                }
            }
            catch (Exception ex)
            {
                ErrorDescription = ex.Message;
            }
            finally
            {
                connection.Close();
            }
            return user;
        }

        public bool UpdateAnnouncement(Announcement ann)
        {
            if (DeleteAnnouncement(ann))
                if (AddAnnouncement(ann))
                    return true;
            return false;
        }


        //Получаем список всех категорий товара
        private List<Category> GetAllCategoriesByAnnouncemetId(int annId)
        {
            var cats = new List<Category>();
            try
            {
                var cmd = new NpgsqlCommand(
                    "SELECT c.* FROM \"Categories\" AS c INNER JOIN \"AnnouncementCategory\" AS ac ON c.\"Id\"=ac.\"CategoriesId\" " +
                    "WHERE ac.\"AnnouncementsId\" = @annid;", connection);
                cmd.Parameters.AddWithValue("@annid", annId);
                using var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    cats.Add(new Category
                    {
                        Id = (int)reader["Id"],
                        Title = reader["Title"]?.ToString()
                    });
                }
            }
            catch (Exception)
            {
                cats = null;
            }
            return cats;
        }

        //Добавляем категорию товара для объявления
        private void AddAnnouncementCategory(int annId, int catId)
        {
            try
            {
                var cmd = new NpgsqlCommand(
                    "INSERT INTO \"AnnouncementsCategory\" (\"AnnouncementsId\", \"CategoriesId\") VALUES(@annid, @catid);", connection);
                cmd.Parameters.AddWithValue("@annid", annId);
                cmd.Parameters.AddWithValue("@catid", catId);
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
            }
        }
        //******************************************************************************************
        /// <summary>
        /// Dispose of unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing">Is already disposed?</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing) connection.Dispose();
            disposed = true;
        }
        ~RepositoryADO()
        {
            Dispose(false);
        }
    }
}
