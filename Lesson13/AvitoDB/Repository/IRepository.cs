﻿using System;
using System.Collections.Generic;

namespace AvitoDB
{
    interface IRepository : IDisposable
    {
        /// <summary>
        /// Информация об ошибке
        /// </summary>
        string ErrorDescription { get; }

        /// <summary>
        /// Наименование
        /// </summary>
        string Title { get; }

        /// <summary>
        /// Получение списка всех пользователей
        /// </summary>
        /// <returns></returns>
        IEnumerable<User> GetAllUsers();

        /// <summary>
        /// Получить пользователя по логину/паролю
        /// </summary>
        User GetUserByLoginPwd(string login, string password);

        /// <summary>
        /// Добавить новое объявление
        /// </summary>
        /// <returns>признак успешного выполнения операции</returns>
        bool AddAnnouncement(Announcement ann);

        /// <summary>
        /// Редактировать объявление
        /// </summary>
        /// <returns>признак успешного выполнения операции</returns>
        bool UpdateAnnouncement(Announcement ann);

        /// <summary>
        /// Удалить объявление по Id
        /// </summary>
        /// <returns>признак успешного выполнения операции</returns>
        bool DeleteAnnouncement(Announcement ann);

        /// <summary>
        /// Получить все объявления пользователя
        /// </summary>
        /// <param name="userId">Id пользователя</param>
        /// <returns>Список всех объявлений пользователя</returns>
        IEnumerable<Announcement> GetAllUserActualAnnouncemets(int userId);

        /// <summary>
        /// Получение категории товара по ее названию
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        Category GetCategoryByTitle(string title);
    }
}
