﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvitoDB
{
    class RepositoryEF : IRepository
    {
        private readonly ApplicationContext db;
        private bool disposed = false;

        public RepositoryEF(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationContext>();
            optionsBuilder.UseNpgsql(connectionString);
            db = new ApplicationContext(optionsBuilder.Options);
        }

        public string ErrorDescription { get; private set; }
        public string Title { get => "Entity Framework Core"; }

        public RepositoryEF(ApplicationContext db)
            => this.db = db;

        public IEnumerable<User> GetAllUsers()
        {
            try
            {
                return db.Users.Include(u => u.City).ToList();
            }
            catch (Exception ex)
            {
                ErrorDescription = ex.Message;
                return null;
            }
        }

        public bool AddAnnouncement(Announcement ann)
        {
            try
            {
                db.Announcements.Add(ann);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                ErrorDescription = ex.Message;
                return false;
            }
        }

        public bool DeleteAnnouncement(Announcement ann)
        {
            try
            {
                db.Announcements.Remove(ann);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                ErrorDescription = ex.Message;
                return false;
            }
        }

        public IEnumerable<Announcement> GetAllUserActualAnnouncemets(int userId)
        {
            try
            {
                return db.Announcements.AsNoTracking().Where(a => a.UserId == userId && a.Actual).ToList();
            }
            catch (Exception ex)
            {
                ErrorDescription = ex.Message;
                return null;
            }
        }

        public User GetUserByLoginPwd(string login, string password)
        {
            try
            {
                return db.Users.Where(u => u.Email == login && u.Password == password).Include(u => u.City).First();
            }
            catch (Exception ex)
            {
                ErrorDescription = ex.Message;
                return null;
            }
        }

        public bool UpdateAnnouncement(Announcement ann)
        {
            try
            {
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                ErrorDescription = ex.Message;
                return false;
            }
        }

        public Category GetCategoryByTitle(string title)
            => db.Categories.Where(c => c.Title == title).FirstOrDefault();

        /// <summary>
        /// Dispose of unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing">Is already disposed?</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing) db.Dispose();
            disposed = true;
        }
        ~RepositoryEF()
        {
            Dispose(false);
        }
    }
}
