﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AvitoDB
{
    /// <summary>
    /// Объявление
    /// </summary>
    public class Announcement
    {
        /// <summary>
        /// Id объявления
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Наименование товара в объявлении
        /// </summary>
        [Required, MaxLength(100)]
        public string Title { get; set; }

        /// <summary>
        /// Подробное описание товара
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Дата публикации
        /// </summary>
        [Required]
        public DateTime PublicationDate { get; set; }

        /// <summary>
        /// Цена товара
        /// </summary>
        [Required]
        public decimal Price { get; set; }

        /// <summary>
        /// Признак актуальности объявления
        /// </summary>
        [Required]
        public bool Actual { get; set; }

        /// <summary>
        /// Id пользователя, разместившего объявление
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// Пользователь, разместивший объявление
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Категории, в которые входит товар
        /// </summary>
        public List<Category> Categories { get; set; }

        public override string ToString()
            => $"{Title}{(Description == null || Description == "" ? "" : " - " + Description)}, Цена: {Price}, Опубликовано: {PublicationDate.ToString("dd.MM.yyyy")}";
    }
}
