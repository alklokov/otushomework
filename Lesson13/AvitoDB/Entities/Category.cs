﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AvitoDB
{
    /// <summary>
    /// Категория товара
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Id категории товаров
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название категории
        /// </summary>
        [Required, MaxLength(100)]
        public string Title { get; set; }

        /// <summary>
        /// Объявления с товаром данной категории
        /// </summary>
        public List<Announcement> Announcemets { get; set; }

        public override string ToString()
            => Title;
    }
}
