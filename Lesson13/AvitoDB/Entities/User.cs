﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AvitoDB
{
    /// <summary>
    /// Пользователь системы (может быть и покупателем, и продавцом)
    /// </summary>
    public class User
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        [Required, MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// EMail пользователя (является логином в системе)
        /// </summary>
        [Required, MaxLength(100)]
        public string Email { get; set; }

        /// <summary>
        /// Пароль пользователя
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// Телефон пользователя
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Город проживания
        /// </summary>
        public City City { get; set; }

        public List<Review> ReviewsAboutUser { get; set; }

        public List<Review> AuthorOfReviews { get; set; }

        public override string ToString()
            => $"{Name}, E-Mail:{Email}" + (Phone == null || Phone == "" ? "" : $", Phone: {Phone}") + (City == null ? "" : $", {City}");
    }
}
