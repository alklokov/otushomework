﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AvitoDB
{
    /// <summary>
    /// Город
    /// </summary>
    public class City
    {
        /// <summary>
        /// Id города
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название города
        /// </summary>
        [Required, MaxLength(100)]
        public string CityName { get; set; }

        /// <summary>
        /// Пользователи, проживающие в городе
        /// </summary>
        public List<User> Users { get; set; }

        public override string ToString()
            => CityName;
    }
}
