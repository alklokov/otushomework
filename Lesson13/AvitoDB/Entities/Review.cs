﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AvitoDB
{
    /// <summary>
    /// Отзывы о пользователе (продавце)
    /// </summary>
    public class Review
    {
        /// <summary>
        /// Id отзыва
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Текст отзыва
        /// </summary>
        [Required]
        public string Text { get; set; }

        /// <summary>
        /// Дата публикации
        /// </summary>
        [Required]
        public DateTime PublicationDate { get; set; }

        /// <summary>
        /// Автор отзыва
        /// </summary>
        [Required]
        public int AuthorId { get; set; }
        public User Author { get; set; }

        /// <summary>
        /// Продавец, о котором отзыв
        /// </summary>
        [Required]
        public int SellerId { get; set; }
        public User Seller { get; set; }

        public override string ToString()
            => $"{PublicationDate}: {Text}";
    }
}
