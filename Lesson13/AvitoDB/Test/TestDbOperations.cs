﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AvitoDB
{
    class TestDbOperations
    {
        private IRepository repository;
        private User currentUser;
        private Announcement ann;
        private readonly string connectionString;

        public TestDbOperations(string conString)
        {
            connectionString = conString;
        }

        public bool InitTestDb()
            => (new TestDbCreator()).CreateTestDb(connectionString);
        
        public void Run(IRepository rep)
        {
            repository = rep;
            Console.WriteLine($"\n***** Тестирование операций БД с помощью {repository.Title} *****\n");

            if (!ReadAllUsers()) return;
            if (!Login()) return;
            if (!AddAnnouncement()) return;
            if (!ReadAllUserAnnouncements()) return;
            if (!UpdateAnnouncement()) return;
            if (!ReadAllUserAnnouncements()) return;
            if (!DeleteUserAnnouncement()) return;
            if (!ReadAllUserAnnouncements()) return;
        }


        private bool ReadAllUsers()
        {
            var users = repository.GetAllUsers();
            if (users == null)
            {
                Console.WriteLine("Не удается прочитать пользователей: " + repository.ErrorDescription);
                return false;
            }
            Console.WriteLine("Все пользователи системы:");
            foreach (var user in users.OrderBy(u => u.Name))
                Console.WriteLine($"   {user}");
            return true;
        }

        private bool Login()
        {
            currentUser = repository.GetUserByLoginPwd("alex@mail.ru", "Alex");
            if (currentUser == null)
            {
                Console.WriteLine("Не удается получить указанного пользователя: " + repository.ErrorDescription);
                return false;
            }
            Console.WriteLine($"Пользователь успешно вошел в систему\n   {currentUser}");
            return true;
        }

        private bool AddAnnouncement()
        {
            ann = new Announcement
            {
                User = currentUser,
                Title = "Эспандер кистевой",
                Price = 10,
                Description = "Почти новый",
                Categories = new List<Category> { repository.GetCategoryByTitle("Спорттовары") }
            };
            if (!repository.AddAnnouncement(ann))
            {
                Console.WriteLine("Не удается добавить объявление: " + repository.ErrorDescription);
                return false;
            }
            Console.WriteLine($"Пользователь {currentUser.Name} успешно добавил объявление:\n   {ann}");
            return true;
        }

        private bool UpdateAnnouncement()
        {
            ann.Description = "Немного потрепанный";
            ann.Price = 9;
            if (!repository.UpdateAnnouncement(ann))
            {
                Console.WriteLine("Не удается изменить объявление: " + repository.ErrorDescription);
                return false;
            }
            Console.WriteLine($"Пользователь {currentUser.Name} успешно изменил объявление:\n   {ann}");
            return true;
        }

        private bool ReadAllUserAnnouncements()
        {
            var annList = repository.GetAllUserActualAnnouncemets(currentUser.Id);
            if (annList == null)
            {
                Console.WriteLine("Не удается прочитать объявления пользователя: " + repository.ErrorDescription);
                return false;
            }
            Console.WriteLine("Все объявления пользователя {currentUser.Name}:");
            foreach (var ann in annList.OrderBy(a => a.PublicationDate))
                Console.WriteLine($"   {ann}");
            return true;
        }

        private bool DeleteUserAnnouncement()
        {
            if (!repository.DeleteAnnouncement(ann))
            {
                Console.WriteLine("Не удается удалить объявление: " + repository.ErrorDescription);
                return false;
            }
            Console.WriteLine($"Пользователь {currentUser.Name} удалил объявление");
            return true;
        }
    }
}
