﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace AvitoDB
{
    class TestDbCreator
    {
        public string ErrorDescription { get; private set; }

        public bool CreateTestDb(string connectionString)
        {
            try
            {
                var optionsBuilder = new DbContextOptionsBuilder<ApplicationContext>();
                optionsBuilder.UseNpgsql(connectionString);
                using (ApplicationContext db = new ApplicationContext(optionsBuilder.Options))
                {
                    db.Database.EnsureDeleted();
                    db.Database.EnsureCreated();

                    var spb = new City { CityName = "Saint-Petersburg" };
                    var mos = new City { CityName = "Moscow" };
                    db.Cities.AddRange(spb, mos);

                    var user1 = new User { Name = "Alex", Password = "Alex", Email = "alex@mail.ru", Phone = "1234567", City = spb };
                    var user2 = new User { Name = "Victor", Password = "123", Email = "vic@mail.ru", City = spb };
                    var user3 = new User { Name = "Petr", Password = "Petr", Email = "petr@mail.ru", Phone = "1234568", City = mos };
                    db.Users.AddRange(user1, user2, user3);

                    var transport = new Category { Title = "Транспортные средства" };
                    var sportGoods = new Category { Title = "Спорттовары" };
                    db.Categories.AddRange(transport, sportGoods);

                    var ann1 = new Announcement
                    {
                        Title = "Велосипед",
                        User = user1,
                        Price = 123,
                        Categories = new List<Category> { transport, sportGoods }
                    };
                    var ann2 = new Announcement
                    {
                        Title = "Гантели 2кг",
                        User = user2,
                        Price = 15,
                        Categories = new List<Category> { sportGoods }
                    };
                    var ann3 = new Announcement
                    {
                        Title = "Автомобиль Лада Приора",
                        User = user3,
                        Price = 15000,
                        Categories = new List<Category> { transport }
                    };
                    db.Announcements.AddRange(ann1, ann2, ann3);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка инициализации БД:\n" + ex.Message);
                return false;
            }
        }
    }
}
