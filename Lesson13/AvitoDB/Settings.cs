﻿using Microsoft.Extensions.Configuration;

namespace AvitoDB
{
    class Settings
    {
        public string DbConnection { get; private set; }
        public Settings(string basePath, string file)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile(file);

            IConfiguration config = builder.Build();

            DbConnection = config[nameof(DbConnection)];
        }
    }
}
