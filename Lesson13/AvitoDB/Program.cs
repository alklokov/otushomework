﻿using System;

namespace AvitoDB
{
    class Program
    {
        static void Main(string[] args)
        {
            var conString = (new Settings(AppContext.BaseDirectory, "appsettings.json")).DbConnection;
            var test = new TestDbOperations(conString);
            if (test.InitTestDb())
            {
                test.Run(new RepositoryEF(conString));
                test.Run(new RepositoryADO(conString));
            }
            Console.ReadLine();
        }
    }
}
