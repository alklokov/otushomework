﻿namespace Import.Core
{
    public interface IDataParser<T>
    {
        T Parse();
    }
}