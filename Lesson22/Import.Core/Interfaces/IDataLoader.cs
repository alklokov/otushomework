﻿namespace Import.Core
{
    public interface IDataLoader
    {
        void LoadData();
    }
}