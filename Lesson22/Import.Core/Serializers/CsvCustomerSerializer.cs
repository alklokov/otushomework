﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Import.Core
{
    public class CsvCustomerSerializer : ISerializer<Customer>
    {
        public void Serialize (Stream stream, IEnumerable<Customer> customers)
        {
            if (stream == null)
                throw new ArgumentNullException();
            if (customers == null || !customers.Any())
                return;

            foreach (var c in customers)
            {
                var arr = Encoding.UTF8.GetBytes($"{c.Id};{c.FullName ?? ""};{c.Email ?? ""};{c.Phone ?? ""}\n");
                stream.Write(arr, 0, arr.Length);
            }
        }

        public Customer Deserialize(string str)
        {
            var arr = str.Split(new char[] { ';', ',' });
            Customer res = null;
            //Пустого Id не может быть
            if (int.TryParse(arr[0], out var id))
                res = new Customer
                {
                    Id = id,
                    FullName = arr[1],
                    Email = arr[2],
                    Phone = arr[3]
                };
            return res;
        }
    }
}
