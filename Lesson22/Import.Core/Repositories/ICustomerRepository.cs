namespace Import.Core
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);
        void AddCustomerArray(Customer[] customers);
    }
}