namespace Import.Core
{
    public interface IDataGenerator
    {
        void Generate();
    }
}