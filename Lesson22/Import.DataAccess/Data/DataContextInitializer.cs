﻿using Import.Core;

namespace Import.Data
{
    //Класс для инициализация пустой БД
    public class DataContextInitializer
    {
        //Инициализация пустой БД
        public static void InitDatabase(ISettings settings)
        {
            using var context = DataContextFactory.CreateDataContext(settings);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
        }
    }
}
