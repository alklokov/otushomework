using System;
using Import.Core;

namespace Import.Data
{
    public class CustomerRepository : ICustomerRepository, IDisposable
    {
        private readonly DataContext context;
        private bool disposed = false;

        public CustomerRepository(ISettings settings)
        {
            context = DataContextFactory.CreateDataContext(settings);
        }

        public void AddCustomer(Customer customer)
        {
            //Add customer to data source   
            context.Customers.Add(customer);
            context.SaveChanges();
        }

        public void AddCustomerArray(Customer[] customers)
        {
            //Add customer to data source   
            context.Customers.AddRange(customers);
            context.SaveChanges();
        }


        /// <summary>
        /// Dispose of unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing">Is already disposed?</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing) context.Dispose();
            disposed = true;
        }
        ~CustomerRepository()
        {
            Dispose(false);
        }
    }
}