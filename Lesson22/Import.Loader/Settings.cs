﻿using Import.Core;
using Microsoft.Extensions.Configuration;
using System;

namespace Import.Loader
{
    /// <summary>
    /// Настройки приложения
    /// </summary>
    public class Settings : ISettings
    {
        private const int defaultDataCount = 1000;
        private const int defaultThreadCount = 1;
        private const string defaultFileTitle = "customers.csv";
        private const string defaultProcessFile =
            "D:\\TEMP\\Parallel\\Import\\Import.DataGenerator.App\\bin\\Debug\\net5.0\\Import.DataGenerator.App.exe";
        private const DbTypes defaultDbType = DbTypes.postgres;

        /// <summary>
        /// Строка подключения для БД PostgreSQL
        /// </summary>
        public string DbConnectionPostgres { get; private set; }
        /// <summary>
        /// Строка подключения для БД SQLite
        /// </summary>
        public string DbConnectionSQLite { get; private set; }
        /// <summary>
        /// Флаг запуска генерации исходного файла в отдельном потоке
        /// </summary>
        public bool GenerateInOtherProcess { get; private set; }
        /// <summary>
        /// Количество записей в генерируемом файле
        /// </summary>
        public int DataCount { get; private set; }
        /// <summary>
        /// Название генерируемого файла с записями клиентов
        /// </summary>
        public string DataFileName { get; set; }
        /// <summary>
        /// Полное имя исполняемого файла для запуска отдельного процесса
        /// </summary>
        public string ProcessFile { get; private set; }
        /// <summary>
        /// Количество потоков для обработки клиентов и записи их в БД
        /// </summary>
        public int ThreadCount { get; private set; }
        /// <summary>
        /// Тип БД (postgres или sqlite)
        /// </summary>
        public DbTypes DbType { get; private set; }

        public Settings(string basePath, string file)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile(file);

            IConfiguration config = builder.Build();
            
            DbConnectionPostgres = config.GetConnectionString(nameof(DbConnectionPostgres));
            DbConnectionSQLite = config.GetConnectionString(nameof(DbConnectionSQLite));
            GenerateInOtherProcess = Convert.ToBoolean(config[nameof(GenerateInOtherProcess)]);
            DataCount = int.TryParse(config[nameof(DataCount)], out var dataCount) ? dataCount : defaultDataCount;
            ThreadCount = int.TryParse(config[nameof(ThreadCount)], out var threadCount) ? threadCount : defaultThreadCount;

            var str = config[nameof(DataFileName)];
            DataFileName = (str == null || str == "") ? defaultFileTitle : str;

            str = config[nameof(ProcessFile)];
            ProcessFile = (str == null || str == "") ? defaultProcessFile : str;

            DbType = GetDbType(config[nameof(DbType)]);
        }

        private DbTypes GetDbType(string typeName)
        {
            switch (typeName.ToLower())
            {
                case "postgres":
                    return DbTypes.postgres;
                case "sqlite":
                    return DbTypes.sqlite;
                default:
                    return defaultDbType;
            }
        }
    }
}
