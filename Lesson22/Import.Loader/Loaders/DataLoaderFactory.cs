﻿namespace Import.Loader
{
    /// <summary>
    /// Фабрика для создания экземпляра загрузчика в зависимости от настроек
    /// </summary>
    class DataLoaderFactory
    {
        public static DataLoader CreateDataLoader(Settings settings)
        {
            if (settings.ThreadCount == 1)
                return new DataLoaderOneThread(settings);
            else
                return new DataLoaderMultiThread(settings);
        }
    }
}
