﻿using Import.Core;
using Import.Data;
using System;
using System.Diagnostics;

namespace Import.Loader
{
    /// <summary>
    /// Базовый абстрактный класс чтения и загрузки данных
    /// </summary>
    abstract class DataLoader
    {
        protected static string[] customerStrings;
        protected Settings settings;

        public DataLoader(Settings settings)
            => this.settings = settings;

        /// <summary>
        /// Запуск загрузки данных из текстового файла в БД
        /// </summary>
        public void LoadData()
        {
            Console.WriteLine($"\nНачало загрузки данных. Количество потоков: {settings.ThreadCount}");

            DataContextInitializer.InitDatabase(settings);      //Готовим пустую БД
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            ProceedCustomers();
            stopwatch.Stop();

            Console.WriteLine($"Загрузка данных завершена за {stopwatch.Elapsed}");
        }

        /// <summary>
        /// Обработка всех пользователей (чтение, десериализация и запись в БД)
        /// </summary>
        protected abstract void ProceedCustomers();
    }
}
