﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Import.Core;
using Newtonsoft.Json;

namespace WebApiTest
{
    class WebApiTest
    {
        private HttpClient client;

        public WebApiTest(string server)
        {
            client = new HttpClient() { BaseAddress = new Uri(server) };
        }

        public async Task Run()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Тестирование работы с клиентами через WebApi. Выберите действие:\n" +
                    "  'a' - добавление клиента\n" +
                    "  'g' - получение существующего клиента");
                var str = Console.ReadLine();
                switch (str)
                {
                    case "a":
                        await AddCustomer();
                        break;
                    case "g":
                        await GetCustomer();
                        break;
                    default:
                        return;
                }
                Console.WriteLine("Нажмите Enter для продолжения работы");
                Console.ReadLine();
            }
        }

        private async Task GetCustomer()
        {
            var id = InputIntField("Укажите Id клиента:");
            try
            {
                HttpResponseMessage response = await client.GetAsync($"api/customer/{id}");
                if (response.IsSuccessStatusCode)
                {
                    var customer = JsonConvert.DeserializeObject<Customer>(await response.Content.ReadAsStringAsync());
                    Console.WriteLine($"Данные клиента:\n{customer}");
                }
                else
                {
                    Console.WriteLine($"Ошибка выполнения запроса {response.StatusCode}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Ошибка выполнения запроса {ex.Message}");
            }
        }

        private async Task AddCustomer()
        {
            var customer = InputCustomer();
            var content = new StringContent(
                JsonConvert.SerializeObject(customer), 
                Encoding.UTF8, 
                "application/json");
            try
            {
                HttpResponseMessage response = await client.PostAsync("api/customer", content);
                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"Операция выполнена успешно. Данные нового клиента:\n{customer}");
                }
                else
                {
                    Console.WriteLine($"Ошибка выполнения запроса {response.StatusCode}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Ошибка выполнения запроса {ex.Message}");
            }
        }


        //Ввод данных нового клиента
        private Customer InputCustomer()
        {
            return new Customer()
            {
                Id = InputIntField("Введите Id клиента:"),
                FullName = InputStringField("Введите имя:"),
                Email = InputStringField("Введите EMail:"),
                Phone = InputStringField("Введите телефон:")
            };
        }

        private string InputStringField(string title)
        {
            Console.WriteLine(title);
            return Console.ReadLine();
        }

        private int InputIntField(string title)
        {
            Console.WriteLine(title);
            if (int.TryParse(Console.ReadLine(), out var i))
                return i;
            return 0;
        }

    }
}
