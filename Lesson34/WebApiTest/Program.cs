﻿using System;
using System.Threading.Tasks;

namespace WebApiTest
{
    class Program
    {
        private const string server = "http://localhost:40957";

        static async Task Main(string[] args)
        {
            var test = new WebApiTest(server);
            await test.Run();
        }
    }
}
