﻿using System;
using System.Diagnostics;
using System.IO;

namespace Import.DataGenerator.App
{
    class Program
    {
        private static string _dataFileName; 
        private static int _dataCount; 
        
        static void Main(string[] args)
        {
            Console.WriteLine($"Генерация файла начата в процессе {Process.GetCurrentProcess().Id}...");

            if (!TryValidateAndParseArgs(args))
            {
                Console.WriteLine("Генерация файла невозможна!");
                return;
            }

            var generator = GeneratorFactory.GetCsvGenerator(_dataFileName, _dataCount);
            
            generator.Generate();

            Console.WriteLine($"Генерация файла в процессе {Process.GetCurrentProcess().Id} завершена");
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                _dataFileName = args[0];
            }
            else
            {
                Console.WriteLine("Ошибка запуска процесса! Первым параметром должно быть имя файла");
                return false;
            }
            
            if (args.Length > 1)
            {
                if (!int.TryParse(args[1], out _dataCount) || _dataCount <= 0)
                {
                    Console.WriteLine("Ошибка запуска процесса! Вторым параметром должно быть положительное число - количество записей");
                    return false;
                }
            }
            return true;
        }
    }
}