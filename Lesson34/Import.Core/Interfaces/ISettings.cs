﻿namespace Import.Core
{
    /// <summary>
    /// Настройки приложения
    /// </summary>
    public interface ISettings
    {
        /// <summary>
        /// Строка подключения для БД PostgreSQL
        /// </summary>
        public string DbConnectionPostgres { get; }
        /// <summary>
        /// Строка подключения для БД SQLite
        /// </summary>
        public string DbConnectionSQLite { get; }
        /// <summary>
        /// Флаг запуска генерации исходного файла в отдельном потоке
        /// </summary>
        public bool GenerateInOtherProcess { get; }
        /// <summary>
        /// Количество записей в генерируемом файле
        /// </summary>
        public int DataCount { get; }
        /// <summary>
        /// Название генерируемого файла с записями клиентов
        /// </summary>
        public string DataFileName { get; set; }
        /// <summary>
        /// Полное имя исполняемого файла для запуска отдельного процесса
        /// </summary>
        public string ProcessFile { get; }
        /// <summary>
        /// Количество потоков для обработки клиентов и записи их в БД
        /// </summary>
        public int ThreadCount { get; }
        /// <summary>
        /// Тип БД (postgres или sqlite)
        /// </summary>
        public DbTypes DbType { get; }
    }
}
