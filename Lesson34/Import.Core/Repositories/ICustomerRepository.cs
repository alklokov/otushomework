using System.Threading.Tasks;

namespace Import.Core
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);
        void AddCustomerArray(Customer[] customers);
        Task<Customer> GetByIdAsync(int id);
        Task AddAsync(Customer customer);
    }
}