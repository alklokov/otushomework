﻿using System.Collections.Generic;
using System.IO;

namespace Import.Core
{
    public interface ISerializer<T> where T : class
    {
        /// <summary>
        /// Сериализует список объектов и записывает их в поток
        /// </summary>
        /// <param name="stream">поток для записи</param>
        /// <param name="tList">список объектов для сериализации</param>
        void Serialize(Stream stream, IEnumerable<T> tList);

        /// <summary>
        /// Десериализует строку в объект указанного типа
        /// </summary>
        /// <returns>Полученный объект</returns>
        T Deserialize(string str);
    }
}
