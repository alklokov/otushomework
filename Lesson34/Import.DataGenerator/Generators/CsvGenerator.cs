using Import.Core;

namespace Import.DataGenerator
{
    public class CsvGenerator : BaseGenerator
    {
        public CsvGenerator(string fileName, int dataCount) : base(fileName, dataCount, new CsvCustomerSerializer()) { }
    }
}