using Import.Core;

namespace Import.DataGenerator
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetCsvGenerator(string fileName, int dataCount)
        {
            return new CsvGenerator(fileName, dataCount);
        }
    }
}