using Import.Core;
using System.IO;

namespace Import.DataGenerator
{
    public abstract class BaseGenerator : IDataGenerator
    {
        protected readonly string _fileName;
        protected readonly int _dataCount;
        protected readonly ISerializer<Customer> serializer;

        protected BaseGenerator(string fileName, int dataCount, ISerializer<Customer> serializer)
        {
            _fileName = fileName;
            _dataCount = dataCount;
            this.serializer = serializer;
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            using var stream = File.Create(_fileName);
            serializer.Serialize(stream, customers);
        }
    }
}