﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Import.Core;

namespace Import.DataGenerator
{
    [XmlRoot("Customers")]
    public class CustomersList
    {
        public List<Customer> Customers { get; set; }
    }
}