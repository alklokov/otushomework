﻿using Import.Core;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace Import.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerRepository repository;

        public CustomerController(ICustomerRepository repository)
            => this.repository = repository;

        // Получение сотрудника по Id
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var customer = await repository.GetByIdAsync(id); 
            if (customer == null)
                return NotFound();
            return Ok(customer);
        }

        // Добавление сотрудника
        [HttpPost]
        public async Task<IActionResult> Post( Customer customer)
        {
            if (customer == null) 
                return BadRequest("Неверные данные клиента!");
            if (customer.Id <= 0)
                return BadRequest("Неверный Id клиента!");
            if (await repository.GetByIdAsync(customer.Id) != null) 
                return Conflict("Клиент с данным Id уже существует!");
            await repository.AddAsync(customer);
            return Ok();
        }
    }
}
