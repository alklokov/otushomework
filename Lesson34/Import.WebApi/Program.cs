using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System.IO;

namespace Import.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //������ ��� ��������� �� ����� ������������
            Settings.settings = new Settings(Directory.GetCurrentDirectory(), "appsettings.json");

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
