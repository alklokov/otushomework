﻿using Import.Core;
using Microsoft.EntityFrameworkCore;
using System;

namespace Import.Data
{
    /// <summary>
    /// Фабрика для создания экземпляра контекста данных в зависимости от настроек
    /// </summary>
    public class DataContextFactory
    {
        public static DataContext CreateDataContext(ISettings settings)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DataContext>();
            switch (settings.DbType)
            {
                case DbTypes.postgres:
                    return new DataContext(optionsBuilder.UseNpgsql(settings.DbConnectionPostgres).Options);
                case DbTypes.sqlite:
                    return new DataContext(optionsBuilder.UseSqlite(settings.DbConnectionSQLite).Options);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
