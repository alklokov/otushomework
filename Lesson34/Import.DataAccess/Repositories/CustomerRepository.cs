using System;
using System.Linq;
using System.Threading.Tasks;
using Import.Core;
using Microsoft.EntityFrameworkCore;

namespace Import.Data
{
    public class CustomerRepository : ICustomerRepository, IDisposable
    {
        private readonly DataContext context;
        private bool disposed = false;

        public CustomerRepository(ISettings settings)
        {
            context = DataContextFactory.CreateDataContext(settings);
        }

        public void AddCustomer(Customer customer)
        {
            //Add customer to data source   
            context.Customers.Add(customer);
            context.SaveChanges();
        }

        public void AddCustomerArray(Customer[] customers)
        {
            //Add customer to data source   
            context.Customers.AddRange(customers);
            context.SaveChanges();
        }

        public Customer GetById(int id)
        {
            return context.Customers.Where(c => c.Id == id).FirstOrDefault();
        }

        public async Task<Customer> GetByIdAsync(int id)
        {
            return await context.Customers.Where(c => c.Id == id).FirstOrDefaultAsync();
        }

        public async Task AddAsync(Customer customer)
        {
            await context.Customers.AddAsync(customer);
            await context.SaveChangesAsync();
        }


        /// <summary>
        /// Dispose of unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// Protected implementation of Dispose pattern.
        /// </summary>
        /// <param name="disposing">Is already disposed?</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;

            //������-�� �� ������ SaveChangesAsync ��� ����� ��������� �������� ���� � �������:
            //"Can't close, connection is in state Connecting". � ���������� �� ������������� ���� try-catch
            //�������� ������ ��� ������
            //if (disposing) context.Dispose();
            disposed = true;
        }

        ~CustomerRepository()
        {
            Dispose(false);
        }
    }
}