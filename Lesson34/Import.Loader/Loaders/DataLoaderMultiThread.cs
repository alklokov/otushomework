﻿using Import.Core;
using Import.Data;
using System.IO;
using System.Threading.Tasks;

namespace Import.Loader
{
    /// <summary>
    /// Чтение и загрузка данных в многопоточном режиме
    /// </summary>
    class DataLoaderMultiThread : DataLoader
    {
        public DataLoaderMultiThread(Settings settings) : base(settings) { }

        /// <summary>
        /// Обработка всех пользователей (чтение, десериализация и запись в БД)
        /// </summary>
        protected override void ProceedCustomers()
        {
            customerStrings = File.ReadAllLines(settings.DataFileName);
            int blockLen = customerStrings.Length / settings.ThreadCount;
            var tasks = new Task[settings.ThreadCount];
            for (var threadIndex = 0; threadIndex < settings.ThreadCount; threadIndex++)
            {
                //Определяем границы в массиве для обработки в очередном отдельном потоке
                var begin = blockLen * threadIndex;
                var end = blockLen * (threadIndex + 1);
                if (threadIndex == settings.ThreadCount - 1)
                    end = customerStrings.Length;       //Последний интервал до самого конца массива
                //Запускаем обработу в отдельном потоке
                tasks[threadIndex] = Task.Run(() => ProceedCustomersTask(begin, end));
            }
            Task.WaitAll(tasks);
        }

        //Отдельный процесс - Обработка массива в диапазоне [begin, end)
        private void ProceedCustomersTask(int begin, int end)
        {
            ISerializer<Customer> serializer = new CsvCustomerSerializer();
            //Для каждого процесса открывается отдельный контекст данных во избежание ошибок
            var repository = new CustomerRepository(settings);
            for (var i = begin; i < end; i++)
                repository.AddCustomer(serializer.Deserialize(customerStrings[i]));
        }
    }
}
