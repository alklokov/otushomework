﻿using Import.Core;
using Import.Data;
using System.IO;

namespace Import.Loader
{
    /// <summary>
    /// Чтение и загрузка данных в однопоточном режиме
    /// </summary>
    class DataLoaderOneThread : DataLoader
    {
        public DataLoaderOneThread(Settings settings) : base(settings) { }

        /// <summary>
        /// Обработка всех пользователей (чтение, десериализация и запись в БД)
        /// </summary>
        protected override void ProceedCustomers()
        {
            //Читаем все строки исходного файла
            customerStrings = File.ReadAllLines(settings.DataFileName);
            //Десереализуем построчно и сохраняем в БД
            var serializer = new CsvCustomerSerializer();
            var repository = new CustomerRepository(settings);
            for (var i = 0; i < customerStrings.Length; i++)
                repository.AddCustomer(serializer.Deserialize(customerStrings[i]));
        }
    }
}
