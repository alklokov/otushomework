﻿using Import.DataGenerator;
using System;
using System.Diagnostics;
using System.IO;

namespace Import.Loader
{
    /// <summary>
    /// Класс для запуска генерации файла данных
    /// </summary>
    class FileGenerator
    {
        private Settings settings;

        public FileGenerator(Settings settings)
            => this.settings = settings;

        public bool Generate()
        {
            Console.WriteLine("Запуск генерации файла...");
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            if (settings.GenerateInOtherProcess)
                RemoteGenerate();
            else
                LocalGenerate();
            stopwatch.Stop();

            if (File.Exists(settings.DataFileName))
            {
                Console.WriteLine($"Время генерации: {stopwatch.Elapsed}");
                Console.WriteLine($"Количество записей: {settings.DataCount}");
                Console.WriteLine($"Готовый файл: {settings.DataFileName}");
                return true;
            }
            Console.WriteLine($"файл не найден: {settings.DataFileName}");
            Console.WriteLine("Генерация не удалась");
            return false;
        }


        //Локальная генерация файла (в этом же процессе)
        private void LocalGenerate()
        {
            Console.WriteLine($"Генерация файла начата в основном процессе {Process.GetCurrentProcess().Id}...");
            var generator = GeneratorFactory.GetCsvGenerator(settings.DataFileName, settings.DataCount);
            generator.Generate();
            Console.WriteLine($"Генерация файла в основном процессе {Process.GetCurrentProcess().Id} завершена");
        }

        //Генерация файла в отдельном процессе
        private void RemoteGenerate()
        {
            Console.WriteLine($"Из основного процесса {Process.GetCurrentProcess().Id} запущен отдельный процесс генерации файла");
            var handle = StartHandlerProcess();
            handle.WaitForExit();
            Console.WriteLine($"Управление вернулось в основной процесс {Process.GetCurrentProcess().Id}");
        }

        private Process StartHandlerProcess()
        {
            var startInfo = new ProcessStartInfo()
            {
                ArgumentList = { settings.DataFileName, settings.DataCount.ToString() },
                FileName = settings.ProcessFile,
            };

            var process = Process.Start(startInfo);

            return process;
        }
    }
}
