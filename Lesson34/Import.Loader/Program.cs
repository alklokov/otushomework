﻿using System;
using System.IO;

namespace Import.Loader
{
    class Program
    {
        static void Main(string[] args)
        {
            //Читаем все настройки из файла конфигурации
            var settings = new Settings(Directory.GetCurrentDirectory(), "appsettings.json");
            settings.DataFileName = Path.Combine(Directory.GetCurrentDirectory(), settings.DataFileName);

            //Запускаем генерацию файла данных
            var fileGenerator = new FileGenerator(settings);
            if (!fileGenerator.Generate()) return;

            //Загрузка данных из текстового файла в БД
            var loader = DataLoaderFactory.CreateDataLoader(settings);
            loader.LoadData();

            Console.ReadLine();
        }
    }
}