﻿using System;

namespace ArraySumTest
{
    class TestClass
    {
        private long[] array;
        private int maxArrValue;    //Максимальное значение элемента массива
        private Random rnd = new Random();

        public TestClass(int arraySize, int maxValue)
        {
            maxArrValue = maxValue;
            array = new long[arraySize];
            InitArray();
        }

        public void RunTest(IAdder adder)
        {
            Console.WriteLine($"\n{adder.TestInfo}");
            DateTime t1 = DateTime.Now;
            var sum = adder.SumArray(array);
            var time = DateTime.Now - t1;
            Console.WriteLine($"\tРезультат: {sum}\n" +
                              $"\tВремя работы: {time.TotalMilliseconds} мс");
        }

        private void InitArray()
        {
            for (var i = 0; i < array.Length; array[i++] = rnd.Next(maxArrValue));
        }
            
    }
}
