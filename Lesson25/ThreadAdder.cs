﻿using System.Collections.Generic;
using System.Threading;

namespace ArraySumTest
{
    class ThreadAdder : IAdder
    {
        private List<Thread> threads = new List<Thread>();
        private int threadCount;
        private object obj = new object();

        public ThreadAdder(int threadCount)
            => this.threadCount = threadCount;

        public string TestInfo { get => $"Многопоточное суммирование массива ({threadCount} потока)"; }

        public long SumArray(long[] array)
        {
            long res = 0;
            for (var i = 0; i < threadCount; i++)
            {
                var i1 = array.Length / threadCount * i;
                var i2 = (i == threadCount - 1) ? array.Length : array.Length / threadCount * (i + 1);
                var thread = new Thread(() => {
                    for (var i = i1; i < i2; i++)
                    {
                        lock (obj)
                        {
                            res += array[i];
                        }
                    }
                });
                thread.Start();
                threads.Add(thread);
            }
            foreach (var thread in threads)
                thread.Join();
            
            return res;
            
        }
    }
}
