﻿namespace ArraySumTest
{
    class SimpleAdder : IAdder
    {
        public string TestInfo { get; } = "Простое суммирование массива";

        public long SumArray(long[] array)
        {
            long res = 0;
            //res = array.Sum();
            for (var i = 0; i < array.Length; res += array[i++]);
            return res;
        }
    }
}
