﻿using System;

namespace ArraySumTest
{
    interface IAdder
    {
        /// <summary>
        /// Суммирование элементов массива
        /// </summary>
        long SumArray(long[] array);
        /// <summary>
        /// Краткое описание теста
        /// </summary>
        string TestInfo { get; }
    }
}
