﻿using System.Threading.Tasks;

namespace ArraySumTest
{
    class ParallelAdder : IAdder
    {
        private object obj = new object();

        public string TestInfo { get; } = "Параллельное суммирование массива";

        public long SumArray(long[] array)
        {
            long res = 0;
            Parallel.For(0, array.Length,
                (i) =>
                {
                    lock (obj)
                    {
                        res += array[i];
                    }
                });
            return res;
        }
    }
}
