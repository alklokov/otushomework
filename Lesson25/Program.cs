﻿using System;
using System.Collections.Generic;

namespace ArraySumTest
{
    class Program
    {
        const int TestArraySize = 10_000_000;     // Размер тестового массива
        const int MaxArrValue = 999;              //Максимальное значение элемента массива
        const int ThreadCount = 4;                 //Количество потоков
        
        static List<IAdder> adderList = new List<IAdder>();

        static void Main(string[] args)
        {
            InitAdderList();
            var test = new TestClass(TestArraySize, MaxArrValue);
            foreach (var adder in adderList)
                test.RunTest(adder);
        }

        static void InitAdderList()
        {
            adderList.Add(new SimpleAdder());
            adderList.Add(new ParallelAdder());
            adderList.Add(new ThreadAdder(ThreadCount));
        }
    }
}
