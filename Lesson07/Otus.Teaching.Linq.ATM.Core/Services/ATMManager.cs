﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //TODO: Добавить методы получения данных для банкомата

        /// <summary>
        /// Авторизация пользователя по логину и паролю
        /// </summary>
        /// <returns>Id пользователя или 0 в случае ошибки</returns>
        public int AuthoriseUser(string login, string pwd)
        {
            var user = Users.Where(u => u.Login == login && u.Password == pwd).FirstOrDefault();
            if (user == null)
                return 0;
            return user.Id;
        }

        /// <summary>
        /// Получение информации о пользователе по логину и паролю
        /// </summary>
        public string GetUserInfo(int userId)
            => Users.Where(u => u.Id == userId).First().ToString();

        public string GetUserAccountList(int userId)
        {
            var accounts = Accounts.Where(a => a.UserId == userId);
            if (!accounts.Any()) return "У данного пользователя нет открытых счетов";

            var sb = new StringBuilder($"Счета пользователя:\n");
            foreach (var acc in accounts)
                sb.Append($"\t{acc}\n");
            return sb.ToString();
        }

        public string GetUserAccountListWithHistory(int userId)
        {
            var accounts = Accounts.Where(a => a.UserId == userId)
                                   .GroupJoin(History, a => a.Id, h => h.AccountId, (account, history) => new { account, history });
            if (!accounts.Any()) return "У данного пользователя нет открытых счетов";

            var sb = new StringBuilder($"Счета пользователя:\n");
            foreach (var acc in accounts)
            {
                sb.Append($"\t{acc.account} Операции:\n");
                foreach (var hist in acc.history)
                    sb.Append($"\t\t{hist}\n");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Получение данных о всех операциях пополнения счёта с указанием владельца каждого счёта;
        /// </summary>
        public string GetInputCashOperations()
        {
            var operations = from user in Users
                             join acc in Accounts on user.Id equals acc.UserId
                             join oper in History on acc.Id equals oper.AccountId
                             where oper.OperationType == OperationType.InputCash
                             orderby oper.OperationDate
                             select new
                             {
                                 oper.OperationDate,
                                 oper.CashSum,
                                 user.FIO
                             };
            if (!operations.Any()) return "Операций не обнаружено";

            var sb = new StringBuilder($"Все операции пополнения счета:\n");
            var num = 1;
            foreach (var oper in operations)
                sb.Append($"\t{num++}. {oper.OperationDate}. {oper.CashSum}. Владелец: {oper.FIO}\n");
            return sb.ToString();
        }

        /// <summary>
        ///  Получение данных о всех пользователях у которых на счёте сумма больше limit
        /// </summary>
        /// <param name="limit"></param>
        public string GetUsersBySumGreaterThan(decimal limit)
        {
            var usersWithSums = Users
                .GroupJoin(Accounts, u => u.Id, a => a.UserId, (user, acc) => new { user, sum = acc.Sum(a => a.CashAll) })
                .Where(u => u.sum > limit);
            if (!usersWithSums.Any()) return "Пользователей с превышением лимита не обнаружено";

            var sb = new StringBuilder($"Пользователи с превышением лимита ({limit}):\n");
            var num = 1;
            foreach (var us in usersWithSums)
                sb.Append($"\t{num++}. {us.user.FIO}. Сумма по всем счетам: {us.sum}.\n");
            return sb.ToString();
        }
    }
}