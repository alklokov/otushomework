﻿using Otus.Teaching.Linq.ATM.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Otus.Teaching.Linq.Atm
{
    public class AtmUi
    {
        private ATMManager atmManager;
        private int userId;

        public AtmUi(ATMManager atmManager)
            => this.atmManager = atmManager;
        
        public void Run()
        {
            ShowAtmStart();
            ProceedSession();
            ShowAtmStop();
        }

        void ProceedSession()
        {
            string result = "";
            while (true)
            {
                ShowAtmMenu();
                switch (ReadMenuNumber())
                {
                    case AtmOperationCodes.Exit:
                        return;
                    case AtmOperationCodes.UserByLogin:
                        result = GetUserInfo();
                        break;
                    case AtmOperationCodes.UserAccounts:
                        result = GetUserAccountList();
                        break;
                    case AtmOperationCodes.UserAccountsWHistory:
                        result = GetUserAccountListWithHistory();
                        break;
                    case AtmOperationCodes.InputCashOperations:
                        result = atmManager.GetInputCashOperations();
                        break;
                    case AtmOperationCodes.UsersBySum:
                        result = GetUsersBySumGreaterThan();
                        break;
                    case AtmOperationCodes.Error:
                    default:
                        continue;
                }
                Console.Clear();
                Console.WriteLine(result + "\n\nНажмите любую клавишу для продолжения работы");
                Console.ReadLine();
            }
        }

        private void ShowAtmStart()
        {
            Console.Write("Старт приложения-банкомата");
            for (var i = 0; i < 7; i++)
            {
                Thread.Sleep(300);
                Console.Write(".");
            }
            Console.WriteLine();
        }

        private void ShowAtmStop()
        {
            Console.Clear();
            Console.WriteLine("Вы успешно завершили работу с приложением-банкоматом");
        }

        private void ShowAtmMenu()
        {
            Console.Clear();
            Console.WriteLine("\n***** Выберите действие: *****\n" +
                    "1 - Вывод информации о пользователе по логину и паролю\n" +
                    "2 - Вывод данных о всех счетах заданного пользователя\n" +
                    "3 - Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту\n" +
                    "4 - Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта\n" +
                    "5 - Вывод данных о всех пользователях у которых на счёте сумма больше N\n" +
                    "0 - Завершить работу");
        }

        private AtmOperationCodes ReadMenuNumber()
            => Enum.TryParse<AtmOperationCodes>(Console.ReadLine(), out var n) ? n : AtmOperationCodes.Error;

        private bool AuthorizeUser()
        {
            Console.Clear();
            Console.WriteLine("Введите логин:");
            var login = Console.ReadLine();
            Console.WriteLine("Введите пароль:");
            var pwd = Console.ReadLine();

            userId = atmManager.AuthoriseUser(login, pwd);
            return userId != 0;
        }

        private string GetUserInfo()
        {
            if (!AuthorizeUser()) return "Введена неверная комбинация логин/пароль";
            return atmManager.GetUserInfo(userId);
        }

        private string GetUserAccountList()
        {
            if (!AuthorizeUser()) return "Введена неверная комбинация логин/пароль";
            return atmManager.GetUserAccountList(userId);
        }

        private string GetUserAccountListWithHistory()
        {
            if (!AuthorizeUser()) return "Введена неверная комбинация логин/пароль";
            return atmManager.GetUserAccountListWithHistory(userId);
        }

        private string GetUsersBySumGreaterThan()
        {
            Console.Clear();
            Console.WriteLine("Введите сумму лимита:");
            if (decimal.TryParse(Console.ReadLine(), out var limit))
                return atmManager.GetUsersBySumGreaterThan(limit);
            return "Введено недопустимое значение";
        }
    }
}
