﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson09
{
    public class SerializersTest
    {
        private MyClass testClass;
        private string testCsv;
        private string testJson;
        private int testCount;

        public SerializersTest()
        {
            testClass = new MyClass() { MyInt1 = 111, MyInt2 = 222, MyStr1 = "str1", MyStr2 = "str2", MyBool = true };
            testCsv = CsvSerializer.SerializeObject(testClass);
            testJson = JsonConvert.SerializeObject(testClass);
            testCount = 100000;
        }

        public void RunTest()
        {
            Console.WriteLine("Serializable class: " + testClass.ToString());
            Console.WriteLine("CsvSerialized: " + testCsv);
            Console.WriteLine("JsonSerialized: " + testJson);
            Console.WriteLine("Measure Count: " + testCount);

            Console.WriteLine("\nMy Reflection");
            Console.WriteLine("SerializingTime = " + CheckTestTime(CsvSerialize));
            Console.WriteLine("DeserializingTime = " + CheckTestTime(CsvDeserialize));

            Console.WriteLine("\nNewtonSoft");
            Console.WriteLine("SerializingTime = " + CheckTestTime(JsonSerialize));
            Console.WriteLine("DeserializingTime = " + CheckTestTime(JsonDeserialize));
        }

        private string CheckTestTime(Action test)
        {
            DateTime t1 = DateTime.Now;
            test();
            return $"{Convert.ToInt32((DateTime.Now - t1).TotalMilliseconds)} ms";
        }

        private void CsvSerialize()
        {
            string str;
            for (int i = 0; i < testCount; i++)
                str = CsvSerializer.SerializeObject(testClass);
        }

        private void CsvDeserialize()
        {
            MyClass test;
            for (int i = 0; i < testCount; i++)
                test = CsvSerializer.DeserializeObject<MyClass>(testJson);
        }

        private void JsonSerialize()
        {
            string str;
            for (int i = 0; i < testCount; i++)
                str = JsonConvert.SerializeObject(testClass);
        }

        private void JsonDeserialize()
        {
            MyClass test;
            for (int i = 0; i < testCount; i++)
                test = JsonConvert.DeserializeObject<MyClass>(testJson);
        }
    }
}
