﻿using System;

namespace Lesson09
{
    class Program
    {
        private static SerializersTest test;

        static void Main(string[] args)
        {
            test = new SerializersTest();
            test.RunTest();
            Console.ReadLine();
        }
    }
}
