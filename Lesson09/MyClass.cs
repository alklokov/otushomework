﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson09
{
    public class MyClass
    {
        public int MyInt1 { get; set; }
        public int MyInt2 { get; set; }
        public string MyStr1 { get; set; }
        public string MyStr2 { get; set; }
        public bool MyBool { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("TestClass\n");
            foreach (var prop in this.GetType().GetProperties())
            {
                sb.Append($"\t{prop.Name}={prop.GetValue(this)}\n");
            }
            return sb.ToString();
        }
    }
}
