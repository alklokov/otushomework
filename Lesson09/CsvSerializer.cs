﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;
using System.Reflection;

namespace Lesson09
{
    static class CsvSerializer
    {
        public static T DeserializeObject<T>(string csv) where T : class, new()
        {
            T t = new T();
            var type = t.GetType();
            var items = CsvToDictionary(csv);
            foreach ((string name, string val) in items)
            {
                var prop = type.GetProperty(name);
                if (prop == null) continue;
                if (prop.PropertyType == typeof(int))
                {
                    if (int.TryParse(val, out int res))
                        prop.SetValue(t, res);
                }
                else if (prop.PropertyType == typeof(bool))
                {
                    if (bool.TryParse(val, out bool res))
                        prop.SetValue(t, res);
                }
                else if (prop.PropertyType == typeof(string))
                {
                    prop.SetValue(t, val);
                }
            }
            return t;
        }
        public static string SerializeObject(object? obj)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var prop in obj.GetType().GetProperties())
            {
                sb.Append($"{prop.Name}:{prop.GetValue(obj)};");
            }
            return sb.ToString();
        }

        private static Dictionary<string, string> CsvToDictionary(string csv)
        {
            var propArray = csv.Split(new char[] { ';', ',' });
            Dictionary<string, string> propDictionary = new Dictionary<string, string>();
            foreach (var p in propArray)
            {
                string[] pair = p.Split(':', 2, StringSplitOptions.None);
                if (pair.Length == 2)
                    propDictionary.Add(pair[0].Trim(), pair[1]);
            }
            return propDictionary;
        }
    }
}
