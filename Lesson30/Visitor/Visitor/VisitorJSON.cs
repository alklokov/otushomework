﻿namespace Patterns
{
    class VisitorJSON : IVisitor
    {
        public string Serialize(Point p)
            => $"{{ \"x\" : {p.x}, \"y\" : {p.y} }}";

        public string Serialize(Circle p)
            => $"{{ \"x\" : {p.x}, \"y\" : {p.y}, \"r\" : {p.r} }}";

        public string Serialize(Square p)
            => $"{{ \"x\" : {p.x}, \"y\" : {p.y}, \"w\" : {p.w} }}";
    }
}
