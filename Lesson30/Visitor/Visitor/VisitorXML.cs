﻿namespace Patterns
{
    class VisitorXML : IVisitor
    {
        public string Serialize(Point p)
            =>
$@"<POINT>
    <X>{p.x}</X>
    <Y>{p.y}</Y>
</PONT>";

        public string Serialize(Circle p)
            =>
$@"<CIRCLE>
    <X>{p.x}</X>
    <Y>{p.y}</Y>
    <Radius>{p.r}</Radius>
</CIRCLE>";

        public string Serialize(Square p)
            =>
$@"<SQUARE>
    <X>{p.x}</X>
    <Y>{p.y}</Y>
    <Width>{p.w}</Width>
</SQUARE>";
    }
}
