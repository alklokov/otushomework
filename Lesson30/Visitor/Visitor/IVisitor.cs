﻿namespace Patterns
{
    interface IVisitor
    {
        string Serialize(Point p);
        string Serialize(Circle p);
        string Serialize(Square p);
    }
}
