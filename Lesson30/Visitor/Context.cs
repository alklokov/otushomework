﻿using System;

namespace Patterns
{
    class Context
    {
        private IVisitor visitor;

        public void SetVisitor(IVisitor visitor)
            => this.visitor = visitor ?? throw new NullReferenceException();
        

        public string DoSerialization(IElements p)
        {
            if (visitor == null)
                throw new NullReferenceException();
            return p.Accept(visitor);
        }
    }
}
