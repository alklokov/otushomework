﻿using System;

namespace Patterns
{
    class Program
    {
        static void Main(string[] args)
        {
            Test test = new Test();
            test.RunTest();
            Console.ReadLine();
        }
    }
}
