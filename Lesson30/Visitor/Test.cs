﻿using System;

namespace Patterns
{
    class Test
    {
        IElements[] elements =
        {
                new Point(5, 10),
                new Circle(6, 6, 5),
                new Square(10, 10, 20)
        };
        Context context = new Context();

        public void RunTest()
        {
            Console.WriteLine("JSON serialisition using Visitor:");
            OneVisitorTest(new VisitorJSON());
            Console.WriteLine();

            Console.WriteLine("XML serialisition using Visitor:");
            OneVisitorTest(new VisitorXML());
            Console.WriteLine();

            Console.WriteLine("JSON serialisition using Visitor & Strategy:");
            OneVisitorStrategyTest(new VisitorJSON());
            Console.WriteLine();

            Console.WriteLine("XML serialisition using Visitor & Strategy:");
            OneVisitorStrategyTest(new VisitorXML());
            Console.WriteLine();
        }

        private void OneVisitorTest(IVisitor visitor)
        {
            foreach (var item in elements)
                Console.WriteLine(item.Accept(visitor));
        }

        private void OneVisitorStrategyTest(IVisitor visitor)
        {
            context.SetVisitor(visitor);
            foreach (var item in elements)
                Console.WriteLine(context.DoSerialization(item));
        }
    }
}
