﻿namespace Patterns
{
    class Square : Point
    {
        public readonly int w;

        public Square(int x, int y, int w) : base(x, y)
            => this.w = w;

        public override string Accept(IVisitor visitor)
            => visitor.Serialize(this);
    }
}
