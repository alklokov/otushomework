﻿namespace Patterns
{
    interface IElements
    {
        string Accept(IVisitor visitor);
    }
}
