﻿namespace Patterns
{
    class Circle : Point
    {
        public readonly int r;

        public Circle(int x, int y, int r) : base(x, y)
            => this.r = r;

        public override string Accept(IVisitor visitor)
            => visitor.Serialize(this);
    }
}
