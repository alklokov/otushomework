﻿namespace Patterns
{
    class Point : IElements
    {
        public readonly int x, y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public virtual string Accept(IVisitor visitor)
            => visitor.Serialize(this);
    }
}
