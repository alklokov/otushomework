﻿namespace Patterns
{
    interface ISerializer
    {
        string Serialize(Point p);
    }
}
