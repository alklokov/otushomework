﻿using System;

namespace Patterns
{
    class Program
    {
        static void Main(string[] args)
        {
            Point point = new Point(5, 10);
            Context context = new Context();

            context.SetSerializer(new SerializerJSON());
            Console.WriteLine(context.DoSerialization(point));

            context.SetSerializer(new SerializerXML());
            Console.WriteLine(context.DoSerialization(point));

            Console.ReadLine();
        }
    }
}
