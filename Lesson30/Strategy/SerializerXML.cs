﻿namespace Patterns
{
    class SerializerXML : ISerializer
    {
        public string Serialize(Point p)
            =>
$@"<POINT>
    <X>{p.x}</X>
    <Y>{p.y}</Y>
</PONT>";
    }
}
