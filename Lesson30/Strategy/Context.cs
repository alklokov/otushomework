﻿using System;

namespace Patterns
{
    class Context
    {
        private ISerializer serializer;

        public void SetSerializer(ISerializer serializer)
            => this.serializer = serializer ?? throw new NullReferenceException();

        public string DoSerialization(Point p)
        {
            if (p == null)
                throw new NullReferenceException();
            return serializer.Serialize(p);
        }
    }
}
