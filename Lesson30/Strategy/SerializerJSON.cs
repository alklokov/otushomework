﻿namespace Patterns
{
    class SerializerJSON : ISerializer
    {
        public string Serialize(Point p)
            => $"{{ \"x\" : {p.x}, \"y\" : {p.y} }}";
    }
}
